import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { usePeopleContext } from '../contexts/People.context';

export const withContextInitialized = (Component) => (props) => {
  const { initialized: initializedPeople } = usePeopleContext();
  const router = useRouter();

  useEffect(() => {
    console.log('router.route ', router.route);
    if (!initializedPeople && router.route !== '/') {
      router.push('/');
    }
  }, [initializedPeople]);

  return <Component {...props} />;
};
