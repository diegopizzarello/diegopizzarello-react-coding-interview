import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

type Field = 'name' | 'gender' | 'phone' | 'birthday';

const PersonDetail = () => {
  const router = useRouter();
  const [userData, setUserData] = useState(undefined);
  const [hasDataChanged, setHasDataChanged] = useState(false);
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  useEffect(() => {
    load();
  }, []);

  useEffect(() => {
    if (!userData) {
      setUserData(data);
    }
  }, [data]);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const onInputChange = (field: Field, value: string) => {
    setUserData({ ...userData, [field]: value });
    setHasDataChanged(true);
  };

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={() => {}}>
            Edit
          </Button>,
        ]}
      >
        {userData && (
          <>
            <Input
              placeholder="Name"
              value={userData.name}
              onChange={(e) => onInputChange('name', e.target.value)}
            />
            <Input
              placeholder="Gender"
              value={userData.gender}
              onChange={(e) => onInputChange('gender', e.target.value)}
            />
            <Input
              placeholder="Phone"
              value={userData.phone}
              onChange={(e) => onInputChange('phone', e.target.value)}
            />

            <Input
              placeholder="Birthday"
              value={userData.birthday}
              onChange={(e) => onInputChange('birthday', e.target.value)}
            />
          </>
        )}
        {hasDataChanged && (
          <Button type="default" onClick={() => save(userData)}>
            Save
          </Button>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
